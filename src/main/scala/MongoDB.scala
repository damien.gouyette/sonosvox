import configuration.{Configuration, MongoDBConfig}
import domain.User
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.{MongoClient, MongoDatabase}
import zio.{Has, ZLayer, _}


object MongoDB {

  def makeClient: ZIO[Configuration, Nothing,MongoClient] = ZIO.fromFunction{
    conf => MongoClient(conf.get[MongoDBConfig].url)
  }

  val acquireAndReleaseClient: ZLayer[Configuration, Nothing, Has[MongoClient]] = {
    ZLayer.fromAcquireRelease(makeClient)(c => UIO(c.close()))
  }
  val databaseLayer: ZLayer[Has[MongoClient], Throwable, Has[MongoDatabase]] = ZLayer.fromFunction { mongoClient =>
    val codecRegistry = fromRegistries(fromProviders(classOf[User]), DEFAULT_CODEC_REGISTRY)
    mongoClient.get[MongoClient].getDatabase("default").withCodecRegistry(codecRegistry)
  }

  val live = acquireAndReleaseClient >>> databaseLayer
}