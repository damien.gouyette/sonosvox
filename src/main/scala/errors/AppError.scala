package errors


sealed trait AppError {
  val message : String
}
object AppError {
  case class DatabaseAccessError(message : String) extends AppError
  case class DataValidationError(message : String) extends AppError
}
