import pureconfig.ConfigSource
import zio._
import pureconfig.generic.auto._
package object configuration {

  type Configuration = Has[ApiConfig] with Has[MongoDBConfig]

  final case class AppConfig(api: ApiConfig, mongoConfig: MongoDBConfig)
  final case class ApiConfig(endpoint: String, port: Int)
  final case class MongoDBConfig(url: String)

  val apiConfig: URIO[Has[ApiConfig], ApiConfig] = ZIO.service
  val mongoConfig: URIO[Has[MongoDBConfig], MongoDBConfig] = ZIO.service

  object Configuration {
    val live: Layer[Throwable, Configuration] = Task
      .effect(ConfigSource.default.loadOrThrow[AppConfig])
      .map(c => Has(c.api) ++ Has(c.mongoConfig)).toLayerMany
  }

}
