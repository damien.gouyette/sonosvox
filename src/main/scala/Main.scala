
import cats.effect.{ExitCode => CatsExitCode}
import configuration.Configuration
import http.{GameApi, UserApi}
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import repository.GamePersistence.GamePersistence
import repository.{GamePersistence, UserPersistence}
import repository.UserPersistence.UserPersistence
import service.GameService.GameService
import service.{GameService, UserService}
import service.UserService.UserService
import zio.clock.Clock
import zio.console.putStrErr
import zio.interop.catz._
import zio.{RIO, _}

import scala.concurrent.ExecutionContext

object Main extends App {

  type AppEnvironment = Configuration with Clock
    with UserService  with UserPersistence
    with GameService with GamePersistence

  type AppTask[A] = RIO[AppEnvironment, A]

  val appEnvironment = Configuration.live >+> MongoDB.live  >+> UserService.live >+> UserPersistence.live >+> GameService.live >+> GamePersistence.live

  override def run(args: List[String]): ZIO[ZEnv, Nothing, ExitCode] = {
    val program: ZIO[AppEnvironment, Throwable, Unit] =
      for {
        api <- configuration.apiConfig
        httpApp = Router[AppTask](
          "/users" -> UserApi(s"${api.endpoint}/users").route,
          "/games" -> GameApi(s"${api.endpoint}/games").route
        ).orNotFound

        server <- ZIO.runtime[AppEnvironment].flatMap { implicit rts =>
          BlazeServerBuilder[AppTask](executionContext = ExecutionContext.Implicits.global)
            .bindHttp(api.port, api.endpoint)
            .withHttpApp(CORS(httpApp))
            .withoutBanner
            .serve
            .compile[AppTask, AppTask, CatsExitCode]
            .drain
        }
      } yield server

    program
      .provideSomeLayer[ZEnv](appEnvironment)
      .tapError(err => putStrErr(s"Execution failed with: $err"))
      .exitCode
  }


}


