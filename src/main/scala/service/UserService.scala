package service

import domain.User
import org.mongodb.scala.bson.ObjectId
import repository.UserPersistence
import repository.UserPersistence.UserPersistence
import zio.{Has, RIO, ZLayer}
object UserService {

  type UserService = Has[UserService.Service]

  trait Service {
    def createUser(user: User): RIO[UserPersistence , User]
    def findById(_id : ObjectId) : RIO[UserPersistence, Option[User]]
    def list(): RIO[UserPersistence, Seq[User]]
  }

  //service implementation
  val live = ZLayer.succeed(
    new UserService.Service {
      override def createUser(user: User): RIO[UserPersistence, User] =  for {
        user <- UserPersistence.create(user)
      } yield (user)
      override def findById(_id: ObjectId): RIO[UserPersistence, Option[User]] = UserPersistence.findById(_id)
      override def list(): RIO[UserPersistence, Seq[User]] = UserPersistence.list()
    }
  )

  //accessors
  def createUser(user: User): RIO[UserService with UserPersistence , User] = RIO.accessM(_.get.createUser(user))
  def findById(_id : ObjectId): RIO[UserService with UserPersistence, Option[User]] = RIO.accessM(_.get.findById(_id))
  def list(): RIO[UserService with UserPersistence, Seq[User]] = RIO.accessM(_.get.list())

}