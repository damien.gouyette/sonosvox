package service

import domain.{Game, GameStatus, GameType, User}
import errors.AppError
import errors.AppError.{DataValidationError, DatabaseAccessError}
import org.mongodb.scala.bson.ObjectId
import repository.GamePersistence
import repository.GamePersistence.GamePersistence
import zio.{Has, RIO, ZIO, ZLayer}

object GameService {

  type GameService = Has[GameService.Service]

  trait Service {
    def host(user: User, game : GameType): RIO[GamePersistence , Game]
    def findById(_id : ObjectId): RIO[GamePersistence , Option[Game]]
    def join(existingGame: ObjectId, joiningUser: User):ZIO[GameService with  GamePersistence ,AppError, Unit]
  }

  //service implementation
  val live = ZLayer.succeed(
    new GameService.Service {
      override def host(host: User, gameType: GameType): RIO[GamePersistence, Game] = {
        GamePersistence.host(
          Game(None, GameStatus.Created, gameType, host._id, Seq.empty[ObjectId])
        )
      }
      override def findById(_id: ObjectId): RIO[GamePersistence, Option[Game]]  = GamePersistence.findById(_id)
      override def join(existingGameId: ObjectId, joiningUser: User): ZIO[GameService with GamePersistence, AppError, Unit] = {
        for {

          gameOpt <- GamePersistence.findById(existingGameId).mapError(th => DatabaseAccessError(th.getMessage))
          game <- ZIO.fromOption(gameOpt).mapError(s => DataValidationError("Game not found"))
          _ <- GamePersistence.join(existingGameId, joiningUser._id)
            .foldM(
              e => ZIO.fail(DatabaseAccessError(e.getMessage)),
              ZIO.succeed(_)
            )
          _ <- ZIO.fromEither(canJoin(game))
        } yield ()

      }
    }
  )

  def canJoin(game : Game) : Either[DataValidationError, Unit] = {
    if (game.gameType.rangeJoin contains(game.players.size +1)) {
      Right(())
    }else {
      Left(DataValidationError(s"The game is full, only ${game.gameType.rangeJoin.end} allowed"))
    }
  }

  def host(user: User, game: GameType): RIO[GameService with GamePersistence, Game] = RIO.accessM(_.get.host(user, game))
  def findById(_id : ObjectId): RIO[GameService with GamePersistence, Option[Game]] = RIO.accessM(_.get.findById(_id))
  def join(existingGame: ObjectId, joiningUser: User) : ZIO[GameService with  GamePersistence ,AppError, Unit] = RIO.accessM(_.get.join(existingGame, joiningUser))
}