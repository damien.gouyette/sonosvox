package domain

import org.mongodb.scala.bson.{Document, ObjectId}

case class User(_id: ObjectId, email: String, name: String){
  def toDocument = {
    Document("_id" -> _id, "email" -> email, "name" -> name)
  }

}
object User {
  def fromDocument(d: Document) : User  = {
    val name =   d.getString("name")
    val _id = d.getObjectId("_id")
    val email = d.getString("email")
    User(_id, email, name )
  }
}