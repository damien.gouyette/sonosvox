package domain

import org.mongodb.scala.bson.{BsonArray, BsonObjectId, BsonValue, Document, ObjectId}
import scala.jdk.CollectionConverters._

import scala.language.implicitConversions

sealed trait GameType {
  val name : String
  val rangeJoin  : Range
}
case object Chess extends GameType {
  val name = "Chess"
  val rangeJoin = 1 to 1
}
case object SettlersOfCatan extends GameType {
  val name = "Settlers of Catan"
   val rangeJoin: Range = 2 to 3
}
case object Risk extends GameType {
  val name = "Risk"
  val rangeJoin: Range = 2 to 4
}
object GameType {
  def fromStringOrThrow(name : String)= List(Chess, SettlersOfCatan, Risk).find(_.name.equalsIgnoreCase(name)).getOrElse(throw new RuntimeException(s"value not found $name"))
}

sealed trait GameStatus {
  val name : String
}
object GameStatus {
  case object Created extends GameStatus {
    val name  = "created"
  }
  case object Started extends GameStatus {
     val name: String = "started"
  }
  case object Finished extends GameStatus {
    val name : String= "finished"
  }
  def fromStringOrThrow(name : String)= List(Created, Started, Finished).find(_.name.equalsIgnoreCase(name)).getOrElse(throw new RuntimeException(s"value not found $name"))
}

case class Game(_id : Option[ObjectId],status  : GameStatus,  gameType: GameType, host : ObjectId, players : Seq[ObjectId])  {
  def toDocument = Document("_id" -> _id, "gameStatus"-> status.name,  "gameType" -> gameType.name, "host" -> host, "players" -> BsonArray())
}

case class DocumentOps(document: Document) {
  def getOrThrow(key : String): String = document.getString(key)
}
object DocumentOps{
  implicit def documentToOPS(input : Document) : DocumentOps = DocumentOps(input)
}
object Game {
  import DocumentOps._

  def fromDocument(d: Document) : Game  = {
    val _id = Some(d.getObjectId("_id"))
    val host = d.getObjectId("host")
    val gameType = GameType.fromStringOrThrow(d.getOrThrow("gameType"))
    val gameStatus = GameStatus.fromStringOrThrow(d.getOrThrow("gameStatus"))
    val players = d.get[BsonArray]("players").get.getValues.asScala.map(_.asObjectId().getValue).toSeq
    Game(_id, gameStatus,gameType, host, players)
  }
}