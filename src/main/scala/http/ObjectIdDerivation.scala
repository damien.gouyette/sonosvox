package http

import domain.GameType
import io.circe.{Decoder, Encoder, HCursor, Json}
import org.mongodb.scala.bson.ObjectId

import scala.util.Try

object ObjectIdDerivation {
  implicit val encodeObjectId: Encoder[ObjectId] = (a: ObjectId) =>
    Json.fromString(a.toHexString)

  implicit val decodeObjectId: Decoder[ObjectId] = (c: HCursor) => {
    for {
      id <- c.downField("_id").as[String]
    } yield {
      new ObjectId(id)
    }
  }

  implicit val decodeObjectIdFromString: Decoder[ObjectId] = Decoder.decodeString.emapTry { str =>
    Try(new ObjectId(str))
  }

  object ObjectIdVar {
    def unapply(str: String): Option[ObjectId] = Try(new ObjectId(str)).toOption
  }

  object GameVar {
    def unapply(str: String): Option[GameType] = Some(GameType.fromStringOrThrow(str))
  }

}
