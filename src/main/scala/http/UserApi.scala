package http

import ObjectIdDerivation._
import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import repository.UserPersistence.UserPersistence
import service.UserService
import service.UserService.{UserService, createUser}
import zio._
import zio.interop.catz._
final case class UserApi[R <: UserService with UserPersistence](rootUri: String) {

  type UserTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[UserTask, A] = jsonOf[UserTask, A]

  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[UserTask, A] = jsonEncoderOf[UserTask, A]

  val dsl: Http4sDsl[UserTask] = Http4sDsl[UserTask]

  import dsl._

  def route: HttpRoutes[UserTask] = {
    HttpRoutes.of[UserTask] {
      case request@POST -> Root =>
        request.decode[CreateUserDTO] { user =>
          createUser(user)
            .foldM(
              err => InternalServerError(err.getMessage),
              user => Created(user).map(_.withHeaders(Headers.of(Header("Location", rootUri + s"/${user._id.toHexString}")))))
        }
      case GET -> Root / ObjectIdVar(id) => UserService.findById(id).foldM(ex =>InternalServerError(ex.getMessage), userOpt => userOpt.map(Ok(_)).getOrElse(NotFound()))
    }
  }
}