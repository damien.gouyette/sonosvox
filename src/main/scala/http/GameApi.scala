package http

import cats.implicits._
import domain.{GameType, User}
import http.ObjectIdDerivation._
import io.circe.generic.auto._
import io.circe.{Decoder, Encoder}
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.mongodb.scala.bson.ObjectId
import repository.GamePersistence.GamePersistence
import repository.UserPersistence.UserPersistence
import service.GameService.GameService
import service.UserService.UserService
import service.{GameService, UserService}
import zio._
import zio.interop.catz._
final case class GameApi[R <: GameService with GamePersistence with UserService with UserPersistence](rootUri: String) {

  type HostTask[A] = RIO[R, A]

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]): EntityDecoder[HostTask, A] = jsonOf[HostTask, A]

  implicit def circeJsonEncoder[A](implicit decoder: Encoder[A]): EntityEncoder[HostTask, A] = jsonEncoderOf[HostTask, A]

  val dsl: Http4sDsl[HostTask] = Http4sDsl[HostTask]

  import dsl._

  val  toInternalServerError : Throwable => HostTask[Response[HostTask]] = {
    ex => InternalServerError(ex.getMessage)
  }

  def route: HttpRoutes[HostTask] = {
    HttpRoutes.of[HostTask] {
      case request@POST -> Root / GameVar(gameType) =>
        request.decode[CreateGameDTO] { user =>
          userExistsAndHost(gameType, user)
        }

      case GET -> Root  / ObjectIdVar(gameId)=> GameService.findById(gameId).foldM(toInternalServerError, Ok(_))

      case request@PUT -> Root  / ObjectIdVar(gameId) / "join" => {
        request.decode[JoinGameDTO] { user =>
          userExistsAndJoin(gameId, user)
        }
      }
    }
  }

  private def userExistsAndHost(gameType: GameType, user: CreateGameDTO) = {
    UserService.findById(new ObjectId(user.userId)).foldM(
      toInternalServerError,
      _.map { user =>
        host(gameType, user)
      }.getOrElse(NotFound(s"User ${user.userId} not found"))
    )
  }

  private def host(gameType: GameType, user: User) = {
    GameService.host(user, gameType)
      .foldM(
        toInternalServerError,
        createdGame => Created(createdGame).map(_.withHeaders(Headers.of(Header("Location", rootUri + s"/${createdGame._id.map(_.toHexString).orEmpty}")))))
  }

  private def userExistsAndJoin(gameId: ObjectId, user: JoinGameDTO): RIO[R,  Response[HostTask]] = {
    UserService.findById(new ObjectId(user.userId)).foldM(
      toInternalServerError,
      _.map { user => join(gameId, user)
      }.getOrElse(NotFound(s"User ${user.userId} not found"))
    )
  }

  private def join(gameId: ObjectId, user: User): RIO[R, Response[HostTask]] = {
    GameService.join(gameId, user)
      .foldM(error => Conflict(error.message),
        _ => Ok())
  }
}