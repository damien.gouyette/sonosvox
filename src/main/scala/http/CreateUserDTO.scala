package http

import domain.User
import org.mongodb.scala.bson.ObjectId

import scala.language.implicitConversions

case class CreateUserDTO(email: String, name: String)
object CreateUserDTO {
  implicit def dtoToDomain(input : CreateUserDTO) : User = {
    User(new ObjectId(), input.email, input.name)
  }
}

case class CreateGameDTO(userId : String)
case class JoinGameDTO(userId:String)