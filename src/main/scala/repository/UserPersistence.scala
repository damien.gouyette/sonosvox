package repository

import domain.User
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.ObjectId
import org.mongodb.scala.model.Filters._
import zio._

object UserPersistence {

  type UserPersistence = Has[UserPersistence.Service]

  trait Service {
    def create(user: User): Task[User]
    def findById(id: ObjectId): Task[Option[User]]
    def list(): Task[Seq[User]]
  }

  def create(user: User): RIO[UserPersistence, User] = RIO.accessM(_.get.create(user))

  def findById(id: ObjectId): RIO[UserPersistence, Option[User]] = RIO.accessM(_.get.findById(id))
  def list(): RIO[UserPersistence, Seq[User]] = RIO.accessM(_.get.list())


  val live: ZLayer[Has[MongoDatabase], Nothing, Has[Service]] = ZLayer.fromFunction { db =>
    val collectionName = "users"

    val database = db.get[MongoDatabase]
    new Service {
      override def create(user: User): Task[User] = ZIO.fromFuture { implicit ec =>
        for {
          _ <- database
            .getCollection(collectionName)
            .insertOne(user.toDocument).toFuture()
        } yield user
      }

      override def list(): Task[Seq[User]] = Task.succeed(List())

      override def findById(id: ObjectId): Task[Option[User]] = ZIO.fromFuture {
        implicit ec =>
          for {
            doc <- database
              .getCollection(collectionName).find(equal("_id", id)).toFuture()
          } yield doc.headOption.map(User.fromDocument)
      }
    }
  }

}
