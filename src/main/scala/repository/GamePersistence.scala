package repository

import domain.Game
import org.mongodb.scala.MongoDatabase
import org.mongodb.scala.bson.{Document, ObjectId}
import org.mongodb.scala.model.Filters.equal
import zio._

object GamePersistence {
  type GamePersistence = Has[GamePersistence.Service]

  trait Service {
    def host(game : Game): Task[Game]
    def findById(_id : ObjectId): Task[Option[Game]]
    def join(_id: ObjectId, joiningUser: ObjectId) : Task[Unit]
  }

  def host(game : Game): RIO[GamePersistence,Game] = RIO.accessM(_.get.host(game))
  def findById(_id : ObjectId): RIO[GamePersistence,Option[Game]] = RIO.accessM(_.get.findById(_id))
  def join(_id : ObjectId, joiningUserId: ObjectId): RIO[GamePersistence,Unit] = RIO.accessM(_.get.join(_id, joiningUserId))

  val live: ZLayer[Has[MongoDatabase], Nothing, Has[Service]] = ZLayer.fromFunction { db =>
    val collectionName = "games"

    val database = db.get[MongoDatabase]
    new Service {
      override def host(game : Game): Task[Game] = ZIO.fromFuture { implicit ec =>
        val gameWithId = game.copy(_id = Some(new ObjectId()))
        for {
          _ <- database
            .getCollection(collectionName)
            .insertOne(gameWithId.toDocument)
            .toFuture()
        } yield gameWithId
      }

      override def findById(_id: ObjectId): Task[Option[Game]] = ZIO.fromFuture{ implicit ec =>
        for {
          doc <- database
            .getCollection(collectionName)
            .find(equal("_id", _id))
            .first()
            .toFutureOption()
        } yield {
          doc.map(Game.fromDocument)
        }

      }

      override def join(_id: ObjectId, joiningUser: ObjectId): Task[Unit] = ZIO.fromFuture { implicit ec =>
        for {
          _ <- database
            .getCollection(collectionName)
            .updateOne(
              equal("_id", _id),
              Document("$push" -> Document("players"-> joiningUser)) )
            .toFutureOption()
        } yield {
          ()
        }

      }
    }
  }
}
