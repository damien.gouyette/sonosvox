package mocks

import repository.{GamePersistence, UserPersistence}
import zio.test.mock.mockable

@mockable[UserPersistence.Service]
object UserPersistenceMock

@mockable[GamePersistence.Service]
object GamePersistenceMock