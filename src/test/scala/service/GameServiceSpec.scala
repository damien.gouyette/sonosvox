package service

import data.TestData._
import domain.GameStatus.Created
import domain.{Chess, Game, Risk}
import mocks.GamePersistenceMock
import repository.GamePersistence.GamePersistence
import zio.ULayer
import zio.test.Assertion.{equalTo, isUnit}
import zio.test.TestAspect._
import zio.test.mock.Expectation.value
import zio.test.{DefaultRunnableSpec, assert, suite, testM}
object GameServiceSpec extends DefaultRunnableSpec {

  val chessGamePersistenceMock: ULayer[GamePersistence] = GamePersistenceMock.Host(equalTo(expected = (chessGame)), value(chessGameWithId))
  val riskGamePersistenceMock: ULayer[GamePersistence] = GamePersistenceMock.Host(equalTo(expected = (riskGame)), value(riskCreatedGameWithId))

  val joinCreatedPersistenceMock : ULayer[GamePersistence] = GamePersistenceMock.Join(equalTo(expected = (riskCreatedGameId, joinUserId)), value(()))
  val joinStartedPersistenceMock : ULayer[GamePersistence] = GamePersistenceMock.Join(equalTo(expected = (riskCreatedGameId, joinUserId)), value(()))

  val chessGameFull: Game = Game(Some(chessGameFullId), Created, Chess, hostUserId, Seq(joinUserId))
  val joinFullChessGamePersistenceMock : ULayer[GamePersistence] = GamePersistenceMock.Join(equalTo(expected = (chessGameFullId, joinUser2Id)), value(()))

  val findByIdRiskCreatedGame : ULayer[GamePersistence] = GamePersistenceMock.FindById(equalTo(expected =riskCreatedGameId ), value(Some(riskCreatedGameWithId)))

  def spec = suite("GameServiceSpec")(
    testM("1 hosting a chess game should return a game with no players, only the host (the game creator)") {
      for {
        game <- GameService.host(hostUser, Chess)
      } yield assert(game)(equalTo(chessGameWithId))
    }.provideCustomLayer(chessGamePersistenceMock ++ GameService.live),

    testM("2 hosting a Risk game should return a game with no players, only the host (the game creator)") {
      for {
        game <- GameService.host(hostUser, Risk)
      } yield assert(game)(equalTo(riskCreatedGameWithId))
    }.provideLayer(riskGamePersistenceMock  ++ GameService.live)  ,

    testM("3 joining an existing game in created status should be successful") {
      for {
        joinResult <- GameService.join(riskCreatedGameId, joiningUser)
      } yield assert(joinResult)(isUnit)
    }.provideCustomLayer(GameService.live ++ findByIdRiskCreatedGame ++ joinCreatedPersistenceMock)   @@ ignore,

    //TODO pending until fixed :DataValidationError(could not join an already started game (current status=Started))
    testM("4 joining an already started game should return an error ") {
      for {
        joinResult <- GameService.join(riskStartedGameId, joiningUser)
      } yield assert(joinResult)(isUnit)
    }.provideCustomLayer(  joinStartedPersistenceMock ++ GameService.live) @@ ignore,

    testM("5 joining a complete game should fail") {
      for {
        joinResult <- GameService.join(chessGameFullId, joiningUser2)
      } yield assert(joinResult)(isUnit)
    }.provideCustomLayer(joinFullChessGamePersistenceMock  ++ GameService.live) @@ ignore
  )


}
