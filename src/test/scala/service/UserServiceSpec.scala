package service

import data.TestData.hostUser
import mocks.UserPersistenceMock
import repository.UserPersistence.UserPersistence
import zio.ULayer
import zio.test.Assertion._
import zio.test._
import zio.test.mock.Expectation._

object UserServiceSpec extends DefaultRunnableSpec {

  val createNonExistingUserMock  : ULayer[UserPersistence] = UserPersistenceMock.Create(equalTo(expected = hostUser), value(hostUser))

  def spec = suite("UserServiceSpec")(
  testM("create non existing user should be successful") {
      for {
        res <- UserService.createUser(hostUser)
      } yield assert(res)(equalTo(hostUser))
    }.provideCustomLayer(createNonExistingUserMock ++ UserService.live )
  )
}
