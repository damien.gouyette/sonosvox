package http

import http.ObjectIdDerivation.ObjectIdVar
import org.mongodb.scala.bson.ObjectId
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
class ObjectIdVarSpec extends AnyFlatSpec  with Matchers {

  "a valid objectId" should "be properly converted into an ObjectIdVar" in {
    // Given
    val validObjectIdAsHexString = new ObjectId().toHexString

    // When
    val tested: Option[ObjectId] = ObjectIdVar.unapply(validObjectIdAsHexString)

    // Then
    tested shouldBe defined
  }

  "an valid objectId" should "not be  converted " in {
    // Given
    val invalidObjectIdAsHexString = new ObjectId().toHexString.take(10)

    // When
    val tested: Option[ObjectId] = ObjectIdVar.unapply(invalidObjectIdAsHexString)

    // Then
    tested shouldBe empty
  }

}
