package http

import io.circe.{HCursor, Json, JsonObject}
import org.mongodb.scala.bson.ObjectId
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ObjectIdDerivationSpec extends AnyFlatSpec with Matchers {

  "a valid objectId" should "be properly converted into an ObjectIdVar" in {
    // Given
    val validCursor = HCursor.fromJson(
      Json.fromJsonObject(JsonObject(("_id", Json.fromString(new ObjectId().toHexString)))))

    // When
    val tested = ObjectIdDerivation.decodeObjectId(validCursor)

    // Then
    tested shouldBe Symbol("Right")
  }

  "an invalid objectId" should "not be properly converted " in {
    pendingUntilFixed {
      info("unvalid id should be catched")
      // Given
      val invalidCursor = HCursor.fromJson(
        Json.fromJsonObject(JsonObject(("_id", Json.fromString(new ObjectId().toHexString.take(10))))))

      // When
      val tested = ObjectIdDerivation.decodeObjectId(invalidCursor)

      // Then
      tested shouldBe Symbol("Left")
    }
  }


}
