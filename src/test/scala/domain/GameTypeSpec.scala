package domain

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class GameTypeSpec extends AnyFlatSpec  with Matchers {

  "chess name as a string" should "be mapped to right instance type " in {
    // Given
    val chessNameLowerCased = "chess"

    // When
    val tested = GameType.fromStringOrThrow(chessNameLowerCased)

    // Then
    tested shouldBe Chess
  }

  "unknown game name as a string" should "be mapped to none " in {
    // Given
    val unknowGame = "7Wonders"

    // When then
    assertThrows[RuntimeException]{
       GameType.fromStringOrThrow(unknowGame)
    }
  }

}
