package data

import domain.GameStatus._
import domain.{Chess, Game, Risk, User}
import org.mongodb.scala.bson.ObjectId

object TestData {
  val chessCreatedGameId= new ObjectId("000000000000000000000000")
  val riskCreatedGameId = new ObjectId("000000000000000000000001");
  val riskStartedGameId = new ObjectId("000000000000000000000002");


  val hostUserId = new ObjectId("000000000000000000000003")
  val hostUser = User(hostUserId, "host@user.com", "Host user")

  val joinUserId = new ObjectId("000000000000000000000004")
  val joiningUser = User(joinUserId, "joining@user.com", "Joining User")

  val joinUser2Id = new ObjectId("000000000000000000000005")
  val joiningUser2 = User(joinUser2Id, "joining2@user.com", "Joining User 2")

  val chessGameFullId = new ObjectId("000000000000000000000006")

  val chessGame: Game = Game(None, Created, Chess, hostUserId, Seq.empty[ObjectId])
  val chessGameWithId: Game = Game(Some(chessCreatedGameId), Created, Chess, hostUserId, Seq.empty[ObjectId])

  val riskGame: Game = Game(None, Created, Risk,hostUserId, Seq.empty[ObjectId])
  val riskCreatedGameWithId: Game = Game(Some(riskCreatedGameId), Created, Risk,hostUserId, Seq.empty[ObjectId])
  val riskStartedGameWithId: Game = Game(Some(riskStartedGameId), Started, Risk,hostUserId, Seq.empty[ObjectId])
}
