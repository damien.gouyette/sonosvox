name := "sonosvox"

version := "0.1"


scalaVersion := "2.13.4"

val Http4sVersion = "0.21.8"
val CirceVersion = "0.13.0"

val ZIOVersion = "1.0.3"
val PureConfigVersion = "0.14.0"
val ZIOInterop = "2.2.0.1"

resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"

mainClass in reStart := Some("Main")


libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % ZIOVersion,
  "dev.zio" %% "zio-logging" % "0.5.4",
  "com.github.wi101" %% "embroidery" % "0.1.1",


  "dev.zio" %% "zio-interop-cats" % ZIOInterop,


  // Http4s
  "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
  "org.http4s" %% "http4s-circe" % Http4sVersion,
  "org.http4s" %% "http4s-dsl" % Http4sVersion,

  //pure config
  "com.github.pureconfig" %% "pureconfig" % PureConfigVersion,

  // Circe
  "io.circe" %% "circe-generic" % CirceVersion,
  "io.circe" %% "circe-generic-extras" % CirceVersion,


  "commons-validator" % "commons-validator" % "1.7",
  "org.mongodb.scala" %% "mongo-scala-driver" % "4.1.1",

  // log4j
  "org.slf4j" % "slf4j-log4j12" % "1.7.30",

  "dev.zio" %% "zio-test" % ZIOVersion % "test",
  "dev.zio" %% "zio-test-sbt" % ZIOVersion % "test",

  "org.scalactic" %% "scalactic" % "3.2.2",

  "org.scalatest" %% "scalatest" % "3.2.2" % "test",

)
testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework")
scalacOptions ++= Seq(
  "-Ymacro-annotations"
)