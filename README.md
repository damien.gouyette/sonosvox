# How to start


`sbt` and then `~reStart` 


# Test the API

to start, you must create a user (using httpie)

## Create a user

It will respond with a 201 Created with a location, where you will be able to fetch the user
```bash
 http POST localhost:8888/users email=john.doe@gmail.com name=JohnDoe 
```

Response

```bash
HTTP/1.1 201 Created
Content-Length: 80
Date: Fri, 15 Jan 2021 16:27:42 GMT
Location: 127.0.0.1/users/6001c27ebb46c31c436cdd37

{"_id":"6001c27ebb46c31c436cdd37","email":"john.doe@gmail.com","name":"JohnDoe"}
```


## Host a chess game

```bash
http POST  localhost:8888/games/chess userId=6001c27ebb46c31c436cdd37
```
Response : 
```bash
HTTP/1.1 201 Created
Content-Length: 129
Date: Fri, 15 Jan 2021 16:28:24 GMT
Location: 127.0.0.1/games/6001c2a8bb46c31c436cdd38

{"_id":"6001c2a8bb46c31c436cdd38","status":{"Created":{}},"gameType":{"Chess":{}},"host":"6001c27ebb46c31c436cdd37","players":[]}
```

## Join the created chess Game 

```bash
http PUT  localhost:8888/games/6001c2a8bb46c31c436cdd38/join userId=6001c27ebb46c31c436cdd37
```

Response :
```bash
HTTP/1.1 200 OK
Content-Length: 0
Date: Fri, 15 Jan 2021 16:29:47 GMT
```

## Get information about the current game

```bash
http GET  localhost:8888/games/6001c35bbb46c31c436cdd39 
```

```bash
HTTP/1.1 200 OK
Content-Length: 182
Content-Type: application/json
Date: Fri, 15 Jan 2021 16:33:38 GMT

{
    "_id": "6001c35bbb46c31c436cdd39",
    "gameType": {
        "Chess": {}
    },
    "host": "6001c27ebb46c31c436cdd37",
    "players": [
        "6001c3cdbb46c31c436cdd3a"
    ],
    "status": {
        "Created": {}
    }
}
```

